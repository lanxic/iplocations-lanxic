Api Iplocations-lanxic
=============

Requirements
------------
- python 3.x

Installation
------------
- Install Requirements
```
pip install -r requirements.txt
```
- Download BIN database ip2location
- extract to app/geolocations/data

Testing
-------
```
python manage.py runserver
```
for using api geolocations can be access with url in below:

-	/v1/geolocations/ip_address/8.8.8.8
-	/v1/geolocations/autodetect

BIN Databases
-------------
- Download free IP2Location LITE databases at https://lite.ip2location.com
- Download IP2Location sample databases at https://www.ip2location.com/developers
- [DB3.LITE] IP-COUNTRY-REGION-CITY Database

Environment Testing CloudFoundry / IBM Cloud / Heroku
-----------------------------------------------------
- cli at https://github.com/cloudfoundry/cli
- demo at https://api-locations-lanxic.mybluemix.net/v1/geolocations/ip_address/8.8.8.8

Support
-------
Email: lanxic@gmail.com
URL: https://lanxic.my.id
