from flask import Blueprint
from .api import GeoipAutodetectApi, GeoipLocationApi

from . import api

blueprint = Blueprint('geo', __name__, url_prefix='/v1/geolocations')

blueprint.add_url_rule('/ip_address/<ip_addr>',
                       view_func=GeoipLocationApi.as_view('geoipaddress'))
blueprint.add_url_rule('/autodetect',
                       view_func=GeoipAutodetectApi.as_view('geoipautodetect'))
