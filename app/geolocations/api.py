import http.client, json
import IP2Location
import logging
import socket

from flask import Response, request
from flask.views import MethodView

from werkzeug.exceptions import NotFound

from . import _log, _log_ip_miss
from app import app

class GeoipAutodetectApi(MethodView):
    def get(self):
        try:
            IP2LocObj = IP2Location.IP2Location()
            IP2LocObj.open("app/geolocations/data/IP2LOCATION-LITE-DB3.BIN")
            response = IP2LocObj.get_all(request.remote_addr)
            response_data = json.dumps({
                'code' : 0,
                'status' : 'success',
                'message' : 'request done',
                'data' : {
                    'records':[{
                    'code_alpha2': response.country_short.decode(),
                    'name': response.country_long.decode(),
                    'city': response.city.decode(),
                    'region': response.region.decode()
                    }]
                },
            })

            return Response(response_data, status=http.client.OK,
                            mimetype='application/json')
        except IP2Location.socket.error:
            _log.exception("IP Address not found: {}".format(request.remote_addr))
            _log_ip_miss.warning(request.remote_addr)
            raise
        except Exception:
            _log.exception("An unknown error occurred")
            raise NotFound

class GeoipLocationApi(MethodView):
    def get(self,ip_addr=None):
        try:
            IP2LocObj = IP2Location.IP2Location()
            IP2LocObj.open("app/geolocations/data/IP2LOCATION-LITE-DB3.BIN")
            response = IP2LocObj.get_all(ip_addr)
            response_data = json.dumps({
                'code' : 0,
                'status' : 'success',
                'message' : 'request done',
                'data' : {
                    'records':[{
                    'code_alpha2': response.country_short.decode(),
                    'name': response.country_long.decode(),
                    'city': response.city.decode(),
                    'region': response.region.decode()
                    }]
                },
            })

            return Response(response_data, status=http.client.OK,
                            mimetype='application/json')
        except IP2Location.socket.error:
            _log.exception("IP Address not found: {}".format(ip_addr))
            _log_ip_miss.warning(ip_addr)
            raise NotFound
        except Exception:
            _log.exception("An unknown error occurred")
            raise NotFound
