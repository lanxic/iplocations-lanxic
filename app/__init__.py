from flask import Flask
import app as iplocation_lanxic_package

from app.utility.blueprints_loader import BlueprintLoader
from app.helpers_logging import initialize_logging

app = Flask(__name__)
app.config.from_object('config')

initialize_logging(app)

loader = BlueprintLoader(iplocation_lanxic_package)
blueprint_modules = [
    'app.geolocations.blueprints'
]

for module_name in blueprint_modules:
    mod = loader.load_blueprint_module(module_name)
    bps = loader.discover_blueprint_attributes(mod)
    for bp in bps:
        app.register_blueprint(bp)

# from app import api
