import os
from flask import Flask

import logging
from logging.config import dictConfig

__BASEDIR = os.path.abspath(os.path.dirname(__file__))
LOG_BASE_DIR = os.path.join(__BASEDIR, 'logs')

app = Flask(__name__)
app.config.from_object('config')
debug = app.config['DEBUG']

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s - %(levelname)s - %(message)s - %(name)s'
        },
        'ip_miss_format': {'format': '%(message)s'}
    },
    'handlers': {
        'console': {
            'level': 'NOTSET',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        },
        'app_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filename': os.path.join(LOG_BASE_DIR, 'app.log')
        },
        'ip_miss_log_file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'ip_miss_format',
            'filename': os.path.join(LOG_BASE_DIR, 'ip-lookup-misses.log'),
        }
    },
    'loggers': {
        'viatic': {
            'handlers': ['app_log_file']  if not debug else ['console'],
            'propagate': False,
            'level': 'DEBUG' if debug else 'INFO',
        },
        'viatic.ip_address_miss_logger': {
            'handlers': ['ip_miss_log_file'],
            'propagate': False,
            'level': 'INFO',
        }
    }
}

def initialize_logging(flask_app):
    """ Simple method that initializes the logging configuration on our app.

    :param `flask.Flask` flask_app: Our Flask application object.
    :return: No return value.
    """
    # if the logger attribute is not called BEFORE we initialize our
    # logging configuration, then Flask will override our custom-set log
    # settings.. so we just need to issue any log call.
    dictConfig(LOGGING_CONFIG)
    flask_app.logger.info("Application Started -- Mode: ".format(flask_app.debug))
